import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import { ProductosComponent } from './pages/productos/productos.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { IngresoComponent } from './pages/ingreso/ingreso.component';
import { RegistroComponent } from './pages/registro/registro.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'inicio',
        component: InicioComponent
      },
      {
        path: 'productos',
        component: ProductosComponent
      },
      {
        path: 'sobre',
        component: SobreComponent
      },
      {
        path: 'ingreso',
        component: IngresoComponent
      },
      {
        path: 'registro',
        component: RegistroComponent
      },
      {
        path: '',
        redirectTo: 'inicio'
      },
      {
        path: '**',
        redirectTo: 'inicio'
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class AuthRoutingModule { }
