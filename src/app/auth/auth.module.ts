import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './pages/inicio/inicio.component';
import { ProductosComponent } from './pages/productos/productos.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { AuthRoutingModule } from './auth-routing.module';
import { IngresoComponent } from './pages/ingreso/ingreso.component';
import { RegistroComponent } from './pages/registro/registro.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    InicioComponent,
    ProductosComponent,
    SobreComponent,
    IngresoComponent,
    RegistroComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AuthModule { }
