import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  variable: string ='active';

  constructor(activeRoute: ActivatedRoute) {
    console.log(activeRoute);
  }

  ngOnInit(): void {
  }

}
